var utility1=require('./utility1');
var utility2=require('./utility2');

// First Way
console.log(utility1.count(['osheen','arshbeer','sneha']));
console.log(utility1.add(2,3));
console.log(utility1.pi);

// Second Way
console.log(utility2.count(['osheen','arshbeer','sneha']));
console.log(utility2.add(2,3));
console.log(utility2.pi);
console.log(utility2.subtract(2,3));
