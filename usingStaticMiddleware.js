var  express = require("express")
    ,bodyParser = require('body-parser')
    ,app = express()

app.use(bodyParser.urlencoded({ extended: false }));

//Using static middleware
app.use(express.static(__dirname+'/public'));

function homepage(req,res){
    res.send('<html><head><title>Uploading</title></head><body>Welcome to the home page!</body></html>');  
}
app.get('/', homepage);

app.get('/about', function(req, res) {
    res.send('<html><head><title>Uploading</title></head><body>I am the about page!</body></html>'); 
});

app.get('/hello/:name', function(req, res) {
    res.send('hello ' + req.params.name + '!');
});

app.get('/login', function(req, res) {
    res.sendFile('views/login2.html', {root: __dirname });
});

app.post('/login', function(req, res) {
    res.send('In post method of /username - Hello ' + req.body.usrname + '!');
});

app.listen(1337);
console.log("Server is running on port 1337");
