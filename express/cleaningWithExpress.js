var  express = require("express")
    ,bodyParser = require('body-parser')
    ,app = express()

app.use(bodyParser.urlencoded({ extended: false }));

// home page route
app.get('/', function(req, res) {
    res.send('<html><head><title>Uploading</title></head><body>Welcome to the home page!</body></html>');  
});

// about page route
app.get('/about', function(req, res) {
    res.send('<html><head><title>Uploading</title></head><body>I am the about page!</body></html>'); 
});

// parameters in the URL
app.get('/hello/:name', function(req, res) {
    res.send('hello ' + req.params.name + '!');
});

// form
app.get('/login', function(req, res) {
    var htmlContent='<html><head><title>Login</title></head><body><form action="/login" method="POST">' +
                'Username: <input type="text" name="usrname"><br><br>Password: <input type="password" name="password"><br><input type="submit" value="Submit"></form></body></html>';
    res.send(htmlContent);
});

app.post('/login', function(req, res) {
    res.send('In post method of /username - Hello ' + req.body.usrname + '!');
});


app.listen(1337);
console.log("Server is running on port 1337");
