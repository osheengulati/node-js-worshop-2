var events = require('events');
var exampleEventEmitter = new events.EventEmitter();

var eventLister = function(msg){
console.log("this is the message");
};

//adding event listener
exampleEventEmitter.on('someEvent',eventLister);

exampleEventEmitter.emit('someEvent');
exampleEventEmitter.emit('someEvent');

//removing event listener
exampleEventEmitter.removeListener('someEvent',eventLister);

exampleEventEmitter.emit('someEvent');
