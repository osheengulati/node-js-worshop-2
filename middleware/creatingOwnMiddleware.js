var  express = require("express")
    ,bodyParser = require('body-parser')
    ,app = express()
    ,router = express.Router()

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname+'/public'));
app.use(router);
app.use('/',outer());
app.use(notFound());

//Route Handlers
function homepage(req,res){
    res.send('<html><head><title>Uploading</title></head><body>Welcome to the home page!</body></html>');  
}
router.get('/', homepage);

router.get('/about', function(req, res) {
    res.send('<html><head><title>Uploading</title></head><body>I am the about page!</body></html>'); 
});

router.get('/hello/:name', function(req, res) {
    res.send('hello ' + req.params.name + '!');
});

router.get('/login', function(req, res) {
    res.sendFile('views/login2.html', {root: __dirname });
});

router.post('/login', function(req, res) {
    res.send('In post method of /username - Hello ' + req.body.usrname + '!');
});

//Middleware
function outer(options){
    return function inner(req,res,next){
        console.log('Called middleware on path : '+req.path);
        next();
    }
}

function notFound(options){
    return function notFoundInner(req,res,next){
        res.send('404 error');
    }
}

app.listen(1337);
console.log("Server is running on port 1337");
